
console.log('Started', self);
//Install event
self.addEventListener('install', function(event){
      console.log('Installed', event);
    event.waitUntil(
			caches.open('simple-sw-v1').then(function(cache){
                return cache.addAll([
                    './',
                    'offlineJavaScript.js',
                    'js/materialize.min.js',
                    'css/materialize.min.css',
                    'fonts/roboto/Roboto-Regular.woff2',
                    'fonts/roboto/Roboto-Regular.ttf',
                    'fonts/roboto/Roboto-Regular.woff',
                    "https://lh5.ggpht.com/BVB9QyxrXOzeRsvHNHdeomNxHrrNHGCeWce6SMBgb1kq67DY5AJY_zmbbPIfj2qkxg=w300"
                ]);
            })	
		);
});


self.addEventListener('activate', function(event) {
  console.log('Activated', event);
});

self.addEventListener('message', function(event) {
  console.log('Push message received', event);
  var title="Push message";
  
  event.waitUntil(
      self.registration.showNotification(title, {
          body:'You have sumitted your people',
          icon:'images/icon.png',
          tag:'my tag'
      }));
});

//Fetch event
self.addEventListener('fetch', function(event){
    event.respondWith(
        caches.match(event.request).then(function(response){
            return response||fetch(event.request);
        })
    )
});

self.addEventListener('sync', function (event) {
    console.log("Received sync");
    var title = 'Hey, you\'re back online!';
    var body = 'Would you like to return to the form to finish submitting your records?';
    var icon = '/images/uploadicon.png';
    var tag = 'simple-push-demo-notification-tag';

    event.waitUntil(    
        self.registration.showNotification(title, {
            body: body,
            icon: icon,
            tag: tag
        })
        );
});



self.addEventListener('notificationclick', function (event) {
    event.notification.close();
    event.waitUntil(clients.openWindow("/index.html"));
});

